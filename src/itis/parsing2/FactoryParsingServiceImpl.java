package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
        //write your code here

        FileReader fileReader = null;
        try {

            Class<Factory> parkClass = Factory.class;
            Constructor constructor = parkClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            Factory factory = (Factory) constructor.newInstance();
            Field[] fields = factory.getClass().getDeclaredFields();
            Map<String, String> map = new HashMap<>();

            List<String> errors = new ArrayList<>();

            File dir = new File(factoryDataDirectoryPath);
            List<File> lst = new ArrayList<>();

            File file = new File(factoryDataDirectoryPath);
            File[] files = new File[0];
            if (dir.isDirectory()) {
                files = dir.listFiles();
            }

            for (File file1 : files) {
                fileReader = new FileReader(file1);
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                String string = null;

                String parsingString = null;
                bufferedReader.readLine();
                while ((string = bufferedReader.readLine()) != "---") {
                    parsingString = string.substring(string.indexOf(':') + 2, string.length()).replaceAll("\"", "");
                    if (parsingString != null) {
                        if (string.contains("title")) {
                            if (parsingString.equals("")) {
                                parsingString = null;
                                map.put("title", parsingString);
                            } else {
                                map.put("title", parsingString);
                            }
                        }

                        if (string.contains("firstName")) {
                            map.put("firstName", parsingString);
                        }

                        if (string.contains("legalName")) {
                            map.put("legalName", parsingString);

                        }

                        if (string.contains("middleName")) {
                            map.put("middleName", parsingString);

                        }

                        if (string.contains("secondName")) {
                            map.put("secondName", parsingString);

                        }
                    }
                }
            }


            for (Field field : fields) {

                field.setAccessible(true);

                NotBlank notBlank = field.getDeclaredAnnotation(NotBlank.class);
                if (notBlank != null) {
                    try {
                        if (map.get(field.getName()) == null) {
                            errors.add("Пустое поле");
                        } else {
                            if (field.getType() == LocalDate.class) {
                                field.set(factory, LocalDate.parse(map.get(field.getName())));
                            } else {
                                field.set(factory, map.get(field.getName()));
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

                Concatenate concatenate = field.getDeclaredAnnotation(Concatenate.class);

                if (concatenate != null) {
                    String s = concatenate.delimiter();
                    if (concatenate.fieldNames() != null) {
                        if (map.get(concatenate.fieldNames()[0]) != null & map.get(concatenate.fieldNames()[1]) != null) {
                            field.set(factory, map.get(concatenate.fieldNames()[0]));
                            field.set(factory, map.get(concatenate.fieldNames()[1]));
                            field.set(factory, s);
                        } else {
                            System.err.println("FirstName не полученно");
                        }

                    }
                }

            }

            return factory;

        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | IOException e) {
            e.printStackTrace();
        }


        return null;
    }
}
